package com.hcb.springSecurity.handler;

import com.alibaba.fastjson2.JSON;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author szny-hongcb
 * @ClassName MyAuthenticationFailureHandler
 * @description: TODO
 * @date 2024/3/12 18:44
 * @version: 1.0
 */
public class MyAuthenticationFailureHandler  implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        //捕获其异常
        String localizedMessage = exception.getLocalizedMessage();
        //创建结果对象
        HashMap<Object, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("message", "登录失败");
        result.put("data", localizedMessage);
        //转换成json字符串
        String json = JSON.toJSONString(result);

        //返回响应
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().println(json);

    }
}
