package com.hcb.springSecurity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hcb.springSecurity.config.DBUserDetailsManager;
import com.hcb.springSecurity.entity.User;
import com.hcb.springSecurity.mapper.UserMapper;
import com.hcb.springSecurity.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author szny-hongcb
 * @ClassName UserServiceImpl
 * @description: TODO
 * @date 2024/3/10 13:33
 * @version: 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private DBUserDetailsManager dbUserDetailsManager;

    @Override
    public void saveUserDetails(User user) {
        UserDetails userDetails = org.springframework.security.core.userdetails.User
                .withDefaultPasswordEncoder()
                .username(user.getUsername()) //自定义用户名
                .password(user.getPassword()) //自定义密码
                .roles("ROLES")
                .build();
        dbUserDetailsManager.createUser(userDetails);
    }
}
