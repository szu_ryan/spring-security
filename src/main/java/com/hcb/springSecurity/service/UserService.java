package com.hcb.springSecurity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hcb.springSecurity.entity.User;

/**
 * @author szny-hongcb
 * @ClassName UserService
 * @description: TODO
 * @date 2024/3/10 13:32
 * @version: 1.0
 */
public interface UserService extends IService<User> {
    void saveUserDetails(User user);
}
