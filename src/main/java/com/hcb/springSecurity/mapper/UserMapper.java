package com.hcb.springSecurity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hcb.springSecurity.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author szny-hongcb
 * @ClassName UserMapper
 * @description: TODO
 * @date 2024/3/8 17:26
 * @version: 1.0
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}