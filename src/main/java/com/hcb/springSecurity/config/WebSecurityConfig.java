package com.hcb.springSecurity.config;

/**
 * @author szny-hongcb
 * @ClassName WebSecurityConfig
 * @description: TODO
 * @date 2024/3/8 16:59
 * @version: 1.0
 */

import com.alibaba.fastjson2.JSON;
import com.hcb.springSecurity.handler.MyAuthenticationEntryPoint;
import com.hcb.springSecurity.handler.MyAuthenticationFailureHandler;
import com.hcb.springSecurity.handler.MyAuthenticationSuccessHandler;
import com.hcb.springSecurity.handler.MyLogoutSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.HashMap;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableMethodSecurity
public class WebSecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        //authorizeRequests()：开启授权保护
        //anyRequest()：对所有请求开启授权保护
        //authenticated()：已认证请求会自动被授权
        http
                .authorizeRequests(authorize -> authorize
                        //具有USER_LIST权限的用户可以访问/user/list
                        // .requestMatchers(new AntPathRequestMatcher("/user/list")).hasAuthority("USER_LIST")
                        // 具有USER_LIST权限的用户可以访问/user/list//具有USER_LIST权限的用户可以访问/user/list
                        // .requestMatchers(new AntPathRequestMatcher("/user/add")).hasAuthority("USER_ADD")
                        //具有管理员角色的用户可以访问/user/**
                        // .requestMatchers(new AntPathRequestMatcher("/user/**")).hasRole("ADMIN")
                        .anyRequest()
                        .authenticated()
                )
                // .formLogin(withDefaults());//表单授权方式
                // .httpBasic(withDefaults());//基本授权方式
                .formLogin( form -> {
                    form
                            .loginPage("/login").permitAll() //登录页面无需授权即可访问
                            .usernameParameter("username") //自定义表单用户名参数，默认是username
                            .passwordParameter("password") //自定义表单密码参数，默认是password
                            .failureUrl("/login?error") //登录失败的返回地址
                            .successHandler(new MyAuthenticationSuccessHandler())//认证成功时的处理
                            .failureHandler(new MyAuthenticationFailureHandler())//认证失败时的处理
                    ;
                }); //使用表单授权方式
        //关闭csrf攻击防御
        http.csrf((csrf) -> {
            csrf.disable();
        });
        //注销成功时的处理
        http.logout(logout -> {
            logout.logoutSuccessHandler(new MyLogoutSuccessHandler());
        });

        //错误处理
        http.exceptionHandling(exception  -> {
            exception.authenticationEntryPoint(new MyAuthenticationEntryPoint());//请求未认证的接口
            exception.accessDeniedHandler((request, response, e)->{ //请求未授权的接口

                //创建结果对象
                HashMap result = new HashMap();
                result.put("code", -1);
                result.put("message", "没有权限");

                //转换成json字符串
                String json = JSON.toJSONString(result);

                //返回响应
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().println(json);
            });
        });
        return http.build();
    }

    // /**
    //  * 基于内存的账号密码登录
    //  * @author szny-hongcb
    //  * @date 2024/3/10 13:31
    //  **/
    // @Bean
    // public UserDetailsService userDetailsService(){
    //     InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
    //     manager.createUser(User.withDefaultPasswordEncoder().
    //             username("hcb").
    //             password("123").
    //             roles("USERS").
    //             build());
    //     return manager;
    // }


    /**
     * 基于数据库的账号密码登录
     * @author szny-hongcb
     * @date 2024/3/10 13:31
     * 因为DBUserDetailsManager用了@Component注解，所以此处可以不用注入bean了
     **/
    // @Bean
    // public UserDetailsService userDetailsService(){
    //     DBUserDetailsManager manager = new DBUserDetailsManager();
    //     return manager;
    // }
}
