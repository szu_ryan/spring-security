package com.hcb.springSecurity.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hcb.springSecurity.entity.User;
import com.hcb.springSecurity.mapper.UserMapper;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author szny-hongcb
 * @ClassName DBUserDetailsManager
 * @description: TODO
 * @date 2024/3/10 15:14
 * @version: 1.0
 */
@Component
public class DBUserDetailsManager implements UserDetailsManager, UserDetailsPasswordService {

    @Resource
    private UserMapper userMapper;

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        return null;
    }

    @Override
    public void createUser(UserDetails userDetails) {
        User user = new User();
        user.setUsername(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());
        user.setEnabled(true);
        userMapper.insert(user);
    }

    @Override
    public void updateUser(UserDetails user) {

    }

    @Override
    public void deleteUser(String username) {

    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {

    }

    @Override
    public boolean userExists(String username) {
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",username);
        User user = userMapper.selectOne(queryWrapper);
        if(user == null){
            throw new UsernameNotFoundException(username);
        }else {
            Collection<GrantedAuthority> authorities = new ArrayList<>();
            /*authorities.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "USER_ADD";
                }
            });*/
            authorities.add(()->"USER_LIST");
            authorities.add(()->"USER_ADD");
            //用户-权限-资源
           // return  new org.springframework.security.core.userdetails.User(
           //          user.getUsername(),
           //          user.getPassword(),
           //          user.getEnabled(),
           //          true, //用户账号是否过期
           //          true, //用户凭证是否过期
           //          true, //用户是否未被锁定
           //          authorities); //权限列表
            //用户-角色-资源
            // return org.springframework.security.core.userdetails.User
            //         .withUsername(user.getUsername())
            //         .password(user.getPassword())
            //         .roles("USER")
            //         .build();
            // 基于方法的授权
            return org.springframework.security.core.userdetails.User
                    .withUsername(user.getUsername())
                    .password(user.getPassword())
                    .roles("ADMIN")
                    // .authorities("USER_ADD", "USER_UPDATE") //和上行roles一起使用会覆盖roles
                    .build();
        }
    }

}
