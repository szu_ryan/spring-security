package com.hcb.springSecurity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @author szny-hongcb
 * @ClassName Knife4jConfiguration
 * @description: TODO
 * @date 2024/3/10 16:25
 * @version: 1.0
 */
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    /**
     * 接口分类：配置模块一的接口
     * 如果只有一个模块，删掉模块二即可
     * 如果有多个，可以继续配置
     */
    @Bean(value = "exampleOne")
    public Docket exampleOne() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("演示接口")
                        .description("演示接口")
                        .termsOfServiceUrl("http://127.0.0.1:8080")
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("演示接口")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.hcb.springSecurity.controller"))
                .paths(PathSelectors.any())
                .build();
    }

}
