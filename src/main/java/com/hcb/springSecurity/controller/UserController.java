package com.hcb.springSecurity.controller;

import com.hcb.springSecurity.entity.User;
import com.hcb.springSecurity.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author szny-hongcb
 * @ClassName UserController
 * @description: TODO
 * @date 2024/3/10 13:35
 * @version: 1.0
 */

// @Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    public UserService userService;

    //用户必须有 ADMIN 角色 并且 用户名是 admin 才能访问此方法
    @PreAuthorize("hasRole('ADMIN') and authentication.name == 'admin'")
    @GetMapping("/list")
    public List<User> getList(){
        return userService.list();
    }

    //用户必须有 USER_ADD 权限 才能访问此方法
    @PreAuthorize("hasAuthority('USER_ADD')")
    @PostMapping("/add")
    public void addUser(@RequestBody User user){
         userService.saveUserDetails(user);
    }
}
