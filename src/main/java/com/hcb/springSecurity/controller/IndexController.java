package com.hcb.springSecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author szny-hongcb
 * @ClassName IndexController
 * @description: TODO
 * @date 2024/3/5 18:51
 * @version: 1.0
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String index() {
        return "index";
    }
}
