package com.hcb.springSecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author szny-hongcb
 * @ClassName LoginController
 * @description: TODO
 * @date 2024/3/10 17:28
 * @version: 1.0
 */
@Controller
public class LoginController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}